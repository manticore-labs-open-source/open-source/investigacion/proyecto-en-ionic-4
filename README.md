# Creación de proyecto en ionic 4

Antes de continuar, asegúrese de tener la última versión de Node.js y npm instalada.

Para verificar las versiones instaladas, lo haremos de la siguiente forma


```
$ ionic info
```

![ionic-info](./imagenes-ionic4/ionic-info.png)

***En caso de no tener instalado ionic, instalar globalmente de la siguiente forma.***
```
$ npm install -g ionic
```

![ionic-install](./imagenes-ionic4/install-ionic.png)


Crearemos el nuevo proyecto de la siguiente forma:

``` 
$ ionic start prueba tabs type=--angular
```
![ionic-create](./imagenes-ionic4/ionic-create.png)

Al revisar el proyecto creado, podemos ver que se creo con sus rutas respectivas.

![ionic-rutas](./imagenes-ionic4/rutas-proyecto.png)


Iniciar el servidor ionic
```
$ ionic s
```
![ionic-s](./imagenes-ionic4/ionic-s.png)

![ionic-navegador](./imagenes-ionic4/ionic-serve.png)

<a href="https://www.facebook.com/jonathan.parra.56" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Twitter"/> Jonathan Parra </a><br>
<a href="https://www.linkedin.com/in/jonathan-parra-a89a68162/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Jonathan Parra</a><br>
<a href="https://www.instagram.com/Choco_20jp/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> Choco_20jp</a><br>